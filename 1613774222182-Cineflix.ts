import { MigrationInterface, QueryRunner } from 'typeorm';

export class Cineflix1613774222182 implements MigrationInterface {
  name = 'Cineflix1613774222182';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "user" ("id" SERIAL NOT NULL, "username" character varying NOT NULL, "password" character varying NOT NULL, "salt" character varying NOT NULL, "acessLevel" character varying NOT NULL, CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "director" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_b85b179882f31c43324ef124fea" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "movie" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "launchYear" character varying NOT NULL, "directorId" integer, CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "category" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "category_movies_movie" ("categoryId" integer NOT NULL, "movieId" integer NOT NULL, CONSTRAINT "PK_6358e633d866cdf30ea4ed2c631" PRIMARY KEY ("categoryId", "movieId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_72328227e84c00e729e3df4124" ON "category_movies_movie" ("categoryId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_dba5b08aaa7f01d365f818e313" ON "category_movies_movie" ("movieId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "movie" ADD CONSTRAINT "FK_a32a80a88aff67851cf5b75d1cb" FOREIGN KEY ("directorId") REFERENCES "director"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "category_movies_movie" ADD CONSTRAINT "FK_72328227e84c00e729e3df41242" FOREIGN KEY ("categoryId") REFERENCES "category"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "category_movies_movie" ADD CONSTRAINT "FK_dba5b08aaa7f01d365f818e3133" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "category_movies_movie" DROP CONSTRAINT "FK_dba5b08aaa7f01d365f818e3133"`,
    );
    await queryRunner.query(
      `ALTER TABLE "category_movies_movie" DROP CONSTRAINT "FK_72328227e84c00e729e3df41242"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movie" DROP CONSTRAINT "FK_a32a80a88aff67851cf5b75d1cb"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_dba5b08aaa7f01d365f818e313"`);
    await queryRunner.query(`DROP INDEX "IDX_72328227e84c00e729e3df4124"`);
    await queryRunner.query(`DROP TABLE "category_movies_movie"`);
    await queryRunner.query(`DROP TABLE "category"`);
    await queryRunner.query(`DROP TABLE "movie"`);
    await queryRunner.query(`DROP TABLE "director"`);
    await queryRunner.query(`DROP TABLE "user"`);
  }
}
