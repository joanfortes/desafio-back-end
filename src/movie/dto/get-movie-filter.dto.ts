import { IsOptional, IsNotEmpty } from 'class-validator';

export class GetMovieFilterDto {
  @IsOptional()
  @IsNotEmpty()
  search: string;
}
