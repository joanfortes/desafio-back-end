import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { Category } from 'src/category/category.entity';
import { Director } from 'src/director/director.entity';

export class CreateMovieDto {
  @IsNotEmpty()
  @ApiProperty()
  name: string;
  @IsNotEmpty()
  @ApiProperty()
  category: Category;
  @IsNotEmpty()
  @ApiProperty()
  launchYear: string;
  @IsNotEmpty()
  @Type(() => Director)
  @ApiProperty({ type: () => Director })
  director: Director;
}
