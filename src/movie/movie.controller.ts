import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Movie } from './movie.entity';
import { MovieService } from './movie.service';
import { CreateMovieDto } from './dto/create-movie.dto';
import { GetMovieFilterDto } from './dto/get-movie-filter.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';

@Controller('movies')
@ApiTags('movies')
@UseGuards(AuthGuard())
export class MovieController {
  constructor(private movieService: MovieService) {}
  @Get()
  getMovie(
    @Query(ValidationPipe) filterDto: GetMovieFilterDto,
  ): Promise<Movie[]> {
    return this.movieService.getMovie(filterDto);
  }
  @Get('/:id')
  getMovieById(@Param('id', ParseIntPipe) id: number): Promise<Movie> {
    return this.movieService.getMovieById(id);
  }
  @Post()
  @UsePipes(ValidationPipe)
  createMovie(@Body() createMovieDto: CreateMovieDto): Promise<Movie> {
    return this.movieService.createMovie(createMovieDto);
  }

  @Delete('/:id')
  deleteMovie(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.movieService.deleteMovie(id);
  }

  @Patch('/:id/update')
  updateMovie(
    @Param('id', ParseIntPipe) id: number,
    @Body('movie') movie: Movie,
  ): Promise<Movie> {
    return this.movieService.updateMovie(id, movie);
  }
}
