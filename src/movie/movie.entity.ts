import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { Director } from 'src/director/director.entity';
import {
  BaseEntity,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Category } from '../category/category.entity';

@Entity()
export class Movie extends BaseEntity {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Column()
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @OneToMany(() => Category, (Category) => Category.movies)
  @Type(() => Category)
  @ApiProperty({ type: () => Category })
  category: Category;

  @Column()
  launchYear: string;

  @ManyToOne(() => Director, (Director) => Director.movies)
  @Type(() => Movie)
  @ApiProperty({ type: () => Movie })
  director: Director;
}
