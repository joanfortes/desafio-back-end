import { EntityRepository, Repository } from 'typeorm';
import { Movie } from './movie.entity';
import { CreateMovieDto } from './dto/create-movie.dto';
import { GetMovieFilterDto } from './dto/get-movie-filter.dto';

@EntityRepository(Movie)
export class MovieRepository extends Repository<Movie> {
  async getMovie(filterDto: GetMovieFilterDto): Promise<Movie[]> {
    const { search } = filterDto;
    const query = this.createQueryBuilder('movie');

    if (search) {
      query.andWhere('(movie.category LIKE :search)', {
        search: `%${search}%`,
      });
    }

    const movie = await query.getMany();
    return movie;
  }
  async createMovie(createMovieDto: CreateMovieDto): Promise<Movie> {
    const { name, category, launchYear, director } = createMovieDto;
    const movie = new Movie();
    movie.name = name;
    movie.category = category;
    movie.launchYear = launchYear;
    movie.director = director;
    await movie.save();

    return movie;
  }
}
