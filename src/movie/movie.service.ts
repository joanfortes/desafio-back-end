import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Movie } from './movie.entity';
import { MovieRepository } from './movie.repository';
import { CreateMovieDto } from './dto/create-movie.dto';
import { GetMovieFilterDto } from './dto/get-movie-filter.dto';

@Injectable()
export class MovieService {
  constructor(
    @InjectRepository(MovieRepository)
    private movieRepository: MovieRepository,
  ) {}
  async getMovie(filterDto: GetMovieFilterDto): Promise<Movie[]> {
    return this.movieRepository.getMovie(filterDto);
  }

  async getMovieById(id: number): Promise<Movie> {
    const found = await this.movieRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Movie with ID "${id}" not found`);
    }
    return found;
  }

  async createMovie(createMovieDto: CreateMovieDto) {
    return this.movieRepository.createMovie(createMovieDto);
  }
  async deleteMovie(id: number): Promise<void> {
    const result = await this.movieRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Task with ID "${id}" not found`);
    }
  }
  async updateMovie(id: number, updateMovie: Movie): Promise<Movie> {
    const movie = await this.getMovieById(id);
    movie.name = updateMovie.name;
    movie.launchYear = updateMovie.launchYear;
    movie.director = updateMovie.director;
    movie.category = updateMovie.category;
    await movie.save();
    return movie;
  }
}
