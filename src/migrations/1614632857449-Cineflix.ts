import {MigrationInterface, QueryRunner} from "typeorm";

export class Cineflix1614632857449 implements MigrationInterface {
    name = 'Cineflix1614632857449'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "acessLevel"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "acessLevel" character varying NOT NULL`);
    }

}
