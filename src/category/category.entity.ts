import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { Movie } from 'src/movie/movie.entity';
import {
  BaseEntity,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity()
export class Category extends BaseEntity {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Column()
  @ApiProperty()
  name: string;

  @ManyToMany(() => Movie, (Movie) => Movie.category)
  @JoinTable()
  @Type(() => Movie)
  @ApiProperty({ type: () => Movie })
  movies: Movie[];
}
