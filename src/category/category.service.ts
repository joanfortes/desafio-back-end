import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './category.entity';
import { CategoryRepository } from './category.repository';
import { CreateCategoryDto } from './dto/create-category.dto';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryRepository)
    private categoryRepository: CategoryRepository,
  ) {}
  async getCategoryById(id: number): Promise<Category> {
    const found = await this.categoryRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Category with ID "${id}" not found`);
    }
    return found;
  }

  async createCategory(createCategory: CreateCategoryDto) {
    return this.categoryRepository.createCategory(createCategory);
  }

  async deleteCategory(id: number): Promise<void> {
    const result = await this.categoryRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Category with ID "${id}" not found`);
    }
  }
  async updateCategory(
    id: number,
    updateCategory: Category,
  ): Promise<Category> {
    const category = await this.getCategoryById(id);
    category.name = updateCategory.name;
    await category.save();
    return category;
  }
}
