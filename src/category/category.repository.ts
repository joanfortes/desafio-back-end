import { EntityRepository, Repository } from 'typeorm';
import { Category } from './category.entity';
import { CreateCategoryDto } from './dto/create-category.dto';

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  async createCategory(createCategory: CreateCategoryDto): Promise<Category> {
    const { name } = createCategory;
    const category = new Category();
    category.name = name;
    await category.save();

    return category;
  }
}
