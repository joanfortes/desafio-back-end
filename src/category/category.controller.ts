import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UsePipes,
  ValidationPipe,
  Body,
  Delete,
  Patch,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { Category } from './category.entity';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';

@Controller('category')
@ApiTags('categories')
@UseGuards(AuthGuard())
export class CategoryController {
  constructor(private categoryService: CategoryService) {}
  @Get('/:id')
  getCategoryById(@Param('id', ParseIntPipe) id: number): Promise<Category> {
    return this.categoryService.getCategoryById(id);
  }
  @Post()
  @UsePipes(ValidationPipe)
  createCategory(@Body() createCategory: CreateCategoryDto): Promise<Category> {
    return this.categoryService.createCategory(createCategory);
  }

  @Delete('/:id')
  deleteCategory(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.categoryService.deleteCategory(id);
  }

  @Patch('/:id/update')
  updateCategory(
    @Param('id', ParseIntPipe) id: number,
    @Body('category') category: Category,
  ): Promise<Category> {
    return this.categoryService.updateCategory(id, category);
  }
}
