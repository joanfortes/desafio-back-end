import { ConnectionOptions } from 'typeorm';

const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'cineflix',
  entities: [`${__dirname}/../**/*.entity.{ts,js}`],
  migrationsRun: true,
  synchronize: false,
  dropSchema: false,
  migrations: ['src/migrations/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/migrations',
  },
};

export = connectionOptions;
