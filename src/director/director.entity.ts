import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
} from 'typeorm';
import { Movie } from '../movie/movie.entity';

@Entity()
export class Director extends BaseEntity {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Column()
  @ApiProperty()
  name: string;

  @OneToMany(() => Movie, (Movie) => Movie.director)
  @Type(() => Movie)
  @ApiProperty({ type: () => Movie })
  movies: Movie[];
}
