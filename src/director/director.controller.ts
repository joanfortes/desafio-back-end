import {
  Controller,
  Post,
  Get,
  UsePipes,
  ValidationPipe,
  Body,
  Param,
  ParseIntPipe,
  Delete,
  Patch,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { Director } from './director.entity';
import { DirectorService } from './director.service';
import { CreateDirectorDto } from './dto/create-director.dto';

@Controller('director')
@ApiTags('directors')
@UseGuards(AuthGuard())
export class DirectorController {
  constructor(private directorService: DirectorService) {}
  @Get('/:id')
  getDirectorById(@Param('id', ParseIntPipe) id: number): Promise<Director> {
    return this.directorService.getDirectorById(id);
  }
  @Post()
  @UsePipes(ValidationPipe)
  createDirector(
    @Body() createDirectorDto: CreateDirectorDto,
  ): Promise<Director> {
    return this.directorService.createDirector(createDirectorDto);
  }

  @Delete('/:id')
  deleteDirector(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.directorService.deleteDirector(id);
  }

  @Patch('/:id/update')
  updateDirector(
    @Param('id', ParseIntPipe) id: number,
    @Body('director') director: Director,
  ): Promise<Director> {
    return this.directorService.upadateDirector(id, director);
  }
}
