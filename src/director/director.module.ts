import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DirectorController } from './director.controller';
import { DirectorRepository } from './director.repository';
import { DirectorService } from './director.service';

const passportModule = PassportModule.register({ defaultStrategy: 'jwt' });
@Module({
  imports: [passportModule, TypeOrmModule.forFeature([DirectorRepository])],
  controllers: [DirectorController],
  providers: [DirectorService],
})
export class DirectorModule {}
