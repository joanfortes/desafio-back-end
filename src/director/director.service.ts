import { Injectable, NotFoundException } from '@nestjs/common';
import { DirectorRepository } from './director.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateDirectorDto } from './dto/create-director.dto';
import { Director } from './director.entity';

@Injectable()
export class DirectorService {
  constructor(
    @InjectRepository(DirectorRepository)
    private directorRepository: DirectorRepository,
  ) {}

  async getDirectorById(id: number): Promise<Director> {
    const found = await this.directorRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Director with ID "${id}" not found`);
    }
    return found;
  }

  async createDirector(CreateDirectorDto: CreateDirectorDto) {
    return this.directorRepository.createDirector(CreateDirectorDto);
  }

  async deleteDirector(id: number): Promise<void> {
    const result = await this.directorRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Director with ID "${id}" not found`);
    }
  }
  async upadateDirector(
    id: number,
    updateDirector: Director,
  ): Promise<Director> {
    const director = await this.getDirectorById(id);
    director.name = updateDirector.name;
    director.movies = updateDirector.movies;

    return director;
  }
}
