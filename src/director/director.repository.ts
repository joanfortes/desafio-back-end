import { EntityRepository, Repository } from 'typeorm';
import { Director } from 'src/director/director.entity';
import { CreateDirectorDto } from './dto/create-director.dto';

@EntityRepository(Director)
export class DirectorRepository extends Repository<Director> {
  async createDirector(
    createDirectorDto: CreateDirectorDto,
  ): Promise<Director> {
    const { name } = createDirectorDto;
    const director = new Director();
    director.name = name;
    await director.save();
    return director;
  }
}
