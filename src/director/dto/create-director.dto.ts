import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { Movie } from '../../movie/movie.entity';

export class CreateDirectorDto {
  @IsNotEmpty()
  @ApiProperty()
  name: string;
  @IsNotEmpty()
  @Type(() => Movie)
  @ApiProperty({ type: () => Movie })
  movie: Movie[];
}
